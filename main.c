/*
** SCCS ID:	@(#)main.c	1.3	03/15/05
**
** File:	main.c
**
** Author:	K. Reek
**
** Contributor:	Warren R. Carithers
**
** Description:	Dummy main program
*/
#include <uart.h>
#include <x86arch.h>

#include "c_io.h"
#include "startup.h"
#include "support.h"
#include "rngs.h"

// Global Variables
int out_write_pos;	// The current position to write to the terminal in the output buffer
int out_free_pos;	// The next free position in the output buffer
int ier_status;		// Status of IER register

char in_char;		// The character read in
char out_buffer[256];	// The circular out buffer to use

char bell[2];		// Stores bell character
char newline[2];	// Stores newline character
char single_char[2];	// Used to print out single characters

int num_ticks;		// The number of ticks overall
int cum_ticks;		// Cumulative Ticks Counted
int num_rounds;		// Number of rounds

unsigned short have_char = 0;

/*
** Clock ISR
*/
void clock_isr( int vector, int code ) {
	++num_ticks;
	__outb( PIC_MASTER_CMD_PORT, PIC_EOI );
}

/*
** Transmitter/Reciever ISR
*/
void txrx_isr( int vector, int code ) {

	// Read in the status register
	unsigned int status = __inb( UA4_EIR );
	unsigned int priority = status & UA4_EIR_INT_PRI_MASK;
	while (priority != UA4_EIR_NO_INT) {
		//c_printf( "Transmitter/Reciever ISR(%d: %d [%d])\n", vector, code, priority );
		if ( priority == UA4_EIR_LINE_STATUS ) {
			// Read LSR and discard
			__inb(UA4_LSR);
		}

		if ( priority == UA4_EIR_RX_HIGH ) {
			// Read RXD
			in_char = __inb(UA4_RXD);
			have_char = 1;
		}

		if ( priority == UA5_EIR_RX_FIFO_TO ) {
			// Read RXD
			in_char = __inb(UA4_RXD);
			have_char = 1;
		}

		if ( priority == UA4_EIR_TX_LOW ) {
			// Read EIR/Write to TXD
			// Write out one char to trigger another interrupt
			if (out_buffer[out_write_pos] != '\0') {
				//c_putchar(out_buffer[out_write_pos]);
				__outb(UA4_TXD, out_buffer[out_write_pos]);
			} else {
				// disable TX Interrupts (XOR it to turn if off) when done
				ier_status ^= UA4_IER_TX_INT_ENABLE;
				__outb(UA4_IER, ier_status);
			}
			out_write_pos = (out_write_pos + 1) & 255;
			//c_putchar('\n');
		}

		if ( priority == UA4_EIR_MODEM_STATUS ) {
			// Read Modem Status and discard
			__inb(UA4_MSR);
		}

		status = __inb( UA4_EIR );
		priority = status & UA4_EIR_INT_PRI_MASK;
	}

	__outb( PIC_MASTER_CMD_PORT, PIC_EOI );
}

/*
** Initialize the UART and set the global variables initial values
*/
void init( void ) {
	// Initialize Global Variables
	num_ticks = 0;
	out_write_pos = 0;
	out_free_pos = 0;
	have_char = 0;

	newline[0] = '\n';
	newline[1] = '\0';

	bell[0] = '\7';
	bell[1] = '\0';

	single_char[0] = 'a';
	single_char[1] = '\0';

	cum_ticks = 0;
	num_rounds = 0;

	// Configure FIFO Control Register
	__outb(UA4_FCR, 0x00);
	__outb(UA4_FCR, UA5_FCR_FIFO_ENABLED);
	__outb(UA4_FCR, UA5_FCR_FIFO_ENABLED | UA5_FCR_RXSR);
	__outb(UA4_FCR, UA5_FCR_FIFO_ENABLED | UA5_FCR_RXSR | UA5_FCR_TXSR);

	// Disable Interrupt Enable Register for all interrupts
	__outb(UA4_IER, 0x00);

	// Set the data rate for bank 1
	__outb(UA4_LCR, UA4_LCR_BANK1);
	__outb(UA4_LBGD_L, BAUD_LOW_BYTE( BAUD_9600 ));
	__outb(UA4_LBGD_H, BAUD_HIGH_BYTE( BAUD_9600 ));

	// Set the character format in bank 0
	__outb(UA4_LCR, UA4_LCR_BANK0 | UA4_LCR_BITS_8 | UA4_LCR_1_STOP_BIT | UA4_LCR_NO_PARITY);

	// Turn on Interrupt Signal Enable, Data Terminal Ready and Ready to Send
	__outb(UA4_MODEM_CTL, UA4_MCR_ISEN | UA4_MCR_DTR | UA4_MCR_RTS);

	// Install interrupt service routines here
	__install_isr(INT_VEC_TIMER, clock_isr);
	__install_isr(INT_VEC_SERIAL_PORT_1, txrx_isr);

	// Get the IER Status and store it
	ier_status = __inb(UA4_IER);

	// Enable CPU Interrupts
	__asm("sti");

}

/*
** Read a character in from the terminal's keyboard
**
** @return	The character read in with the parity bit stripped
*/
char t_getchar( void ) {
	// enable RX interrupts
	ier_status |= UA4_IER_RX_INT_ENABLE;
	__outb(UA4_IER, ier_status);

	// Get the character
	while (!have_char)
		; 

	have_char = 0;

	// disable RX Interrupts (XOR it to turn if off)
	ier_status ^= UA4_IER_RX_INT_ENABLE;
	__outb(UA4_IER, ier_status);

	return (in_char & 0x7f);
}

/*
** Writes a string to the terminal
** 
** @param str		The null-terminal string to write to the terminal
*/
void t_puts( char *str ) {

	int write_pos = out_free_pos;

	while (*str != '\0') {
		if (*str == '\n') {
			out_buffer[out_free_pos] = '\r';
			out_free_pos = (out_free_pos + 1) & 255;
		}

		out_buffer[out_free_pos] = *str;
		out_free_pos = (out_free_pos + 1) & 255;
		++str;
	}

	out_buffer[out_free_pos] = '\0';
	++write_pos;

	if ((ier_status & UA4_IER_TX_INT_ENABLE) == 0) {
		// enable RX interrupts
		ier_status |= UA4_IER_TX_INT_ENABLE;
		__outb(UA4_IER, ier_status);

		__outb(UA4_TXD, out_buffer[write_pos - 1]);
	}

}

/*
** Prints an integer (incl up tp one decimal) to the terminal
**
** @param num		The number to print
** @param deicmal	If the number contains a decimal in the rightmost position
**			set this to one, otherwise set it to 0
*/
void t_printint( int num, int decimal ) {

	int num_digits = 0;
	int tmp = num;
	while (tmp > 0) {
		tmp = tmp / 10;
		++num_digits;
	}

	int dec_pos = -1;
	if (decimal > 0) {
		++num_digits;
		dec_pos = num_digits - 2;
	}

	char tmp_str[num_digits + 1];
	tmp_str[num_digits] = '\0';
	--num_digits;

	while (num_digits > -1) {
		if (decimal > 0 && dec_pos == num_digits) {
			tmp_str[num_digits] = '.';
		} else {
			tmp_str[num_digits] = (num % 10) + 48;		
			num = num / 10;
		}
		--num_digits;
	}

	t_puts(tmp_str);

}

int main( void ) {

	init();

	t_puts("");
	t_puts("Kevin Allison (kra2178)\n");
	t_puts("Clock is ticking at the default rate\n");

	t_puts("Enter twelve-chacter seed: ");

	int num_read = 0;
	unsigned long seed = 0;
	unsigned long working_val = 0;
	// Read in 12 characters
	while (num_read < 12) {
		char input = t_getchar();

		if (input == '\r' | input == '\n') {
			// send bell and keep working
			single_char[0] = '\7';
		} else {
			single_char[0] = input;

			unsigned long u_input = (unsigned long)input;
			switch (num_read & 3) {
				case 0:
					working_val = u_input;
					break;
				case 1:
					working_val |= (u_input << 8);
					break;
				case 2:
					working_val |= (u_input << 16);
					break;
				case 3:
					working_val |= (u_input << 24);
					seed ^= working_val;
					break;
				default:
					c_puts("How'd we get here???\n");
					
			}

			++num_read;
		}

		t_puts(single_char);
	}

	char wait_for_enter = t_getchar();
	while (wait_for_enter != '\r' && wait_for_enter != '\n') {
		wait_for_enter = t_getchar();	
	}

	// Print \r\n
	t_puts(newline);

	// Seed the generator
	PutSeed(seed);

	// Continue to run until the user enters and n
	int run = 1;
	while (run) {
		++num_rounds;
		t_puts("Press the enter key when you are ready\n");

		wait_for_enter = t_getchar();
		while (wait_for_enter != '\r' && wait_for_enter != '\n') {
			wait_for_enter = t_getchar();	
		}

		t_puts(bell);

		// Get random lowercase char
		int val = Random() * 25.0;
		char rand_char = (char)(val + 97);
		single_char[0] = rand_char;

		t_puts(single_char);
		t_puts(newline);

		// Count the ticks!
		int start_ticks = num_ticks;
		while (t_getchar() != rand_char);
			;
		int end_ticks = num_ticks;

		int cur_ticks = end_ticks - start_ticks;
		cum_ticks += cur_ticks * 10;
		int avg_ticks = cum_ticks / num_rounds;

		t_puts("Current:  ");
		t_printint(cur_ticks, 0);
		t_puts("   Average:  ");
		t_printint(avg_ticks, 1);
		t_puts(" (");
		t_printint(num_rounds, 0);
		t_puts(" rounds)\n");

		t_puts("Another try? ");	
		single_char[0] = t_getchar();
		t_puts(single_char);
		while (single_char[0] != 'n' && single_char[0] != 'y') {
			t_puts(bell);
			t_puts("\nAnother try? ");	
			single_char[0] = t_getchar();
			t_puts(single_char);
		}

		if (single_char[0] == 'n') { 
			run = 0;
		}

		t_puts(newline);
	}

	// Turn off interrupts and halt
	c_puts("Halting...\n");
	__asm("cli");
	while(1);
}
